link: https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex6/mini_ex06/index.html

![](/mini_ex6/images/Skærmbillede%202019-03-19%20kl.%2023.25.34%202.png)

I really struggled with finishing this game, and after passing the deadline of this assignment by several hours, I chose to upload what I had made that far. I keep underestimating how time-consuming and difficult coding can be, so for the next assignment i have to start earlier - especially, if I want to create something with some amount of complexity. Also, I find it hard to compromise, when I got an idea, so I should probably lower my ambitions and instead add some extra at the final stages of the process if possible.

My game is created in a 3D environment. You are controlling a ball running down a pipe, while you have to dodge laser beams, that are randomly spawning. While a made my objects and functions work quite well, the game over function when hitting the lasers never got to work. I thought about using the colliders from the play library, but I was unsure if they could be used in a 3D environment.

At one point, I wanted to use i for loop for selecting my textures, but I got an error message referring to the p5.min.js library: “TypeError: undefined is not an object (evaluating ‘i.width’)” (+ a reference to p5.min.js). It came when I was trying to change the texture of a 3D object. This isn’t the first time that I get this message, and I wonder if I’m making a mistake or if it’s due to some limitations of the p5 library. 
