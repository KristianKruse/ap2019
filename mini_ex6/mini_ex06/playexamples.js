
let rotationOffset = 0;
let rotationOffsetP = 0;
let rotationOffsetRoll = 0;
let rotationSpeed = 0.05;
let pipeLenght = 10000;
let jumpHeight = 10;
let obstacles = [];
let obstnum = 400;
let adding = 1250;
let speed = 40;

let imgSun;
let playerText;


function preload() {
  imgSun = loadImage('assets/sun1.png');
  playerText = loadImage('assets/flappy_ground.png');
}


function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  frameRate(32);

  for (let i = 0; i < obstnum; i++) {
    obstacles[i] = new Obstacle1(1500*i, random(360)*i);
  }
}


function draw() {
  background(20, 15, 0);

  // Light
  directionalLight(255, 50, windowWidth, -1000);
  ambientLight(30);

  // My two main functions
  environment();
  player();

  // Spawning obstacles from class
  for (let i = 0; i < obstnum; i++) {
    push();
    obstacles[i].show();
  }

  // Adding speed continously
  if (frameCount%2 === 1) {
    speed+=0.03;
  }
}



class Obstacle1 {
  constructor(dist, xrot) {
    this.dist = dist
    this.xrot = xrot
  }
  show() {
    push();
    rotateZ(-rotationOffset);
    translate(500, 500, speed*frameCount-(4900+this.dist));
    rotateZ(this.xrot);
    ambientMaterial(0, 250, 250, 100);
    texture(imgSun);
    box(windowWidth/4, windowWidth*4, windowWidth/4);
    pop();
  }
}


function environment() {

  // Arrows and rotation of the environment
  push();
  rotateX(80);
  if (keyIsDown(RIGHT_ARROW)) {
    rotateY(rotationOffset-=rotationSpeed);
  } else if (keyIsDown(LEFT_ARROW)) {
    rotateY(rotationOffset+=rotationSpeed);
  } else {
    rotateY(rotationOffset);
  }

  // Creating the cylindrical environment
  ambientMaterial(100, 150, 250);
  translate(0, 3000 - speed*frameCount%adding);
  strokeWeight(3);
  stroke(100, 150, 250, 255);
  cylinder(windowWidth/2, pipeLenght, 24, 16, false, false);
  pop();

}

// Player/ball
function player() {
  push();
  ambientMaterial(250, 250, 250);
  noStroke();
  translate(0, 500, -500);
  push();

  // Ball rotation
  if (keyIsDown(RIGHT_ARROW)) {
    rotateZ(rotationOffsetP+=rotationSpeed*5.14);
  } else if (keyIsDown(LEFT_ARROW)) {
    rotateZ(rotationOffsetP-=rotationSpeed*5.14);
  } else {
    rotateX(rotationOffsetRoll+=rotationSpeed*speed/18)
    rotateX(rotationOffsetRoll);
    rotateZ(rotationOffsetP);
  }
  
  texture(playerText);
  sphere(windowWidth/15);
  pop();
  pop();
}
