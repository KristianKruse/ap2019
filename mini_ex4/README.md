link: https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex4/Webcam_synthesis/index.html

Edit: I have now created a new version that can be found in the mini_ex5 folder.

It took me some time, but I finally figured out how to make my program run. 

[](https://gitlab.com/KristianKruse/ap2019/blob/master/mini_ex4/Skærmbillede%202019-03-13%20kl.%2011.01.32.png)

If there's no sound when you're running the program, you need to go to your browser setting for "this website" and change the setting "autoplay" to allow all media