// filter and offset
let capture;
let fr = 10;

let offset = 100;
let pixelosc1 = offset*1200 + offset;
let pixelosc2 = offset*1200 + (1200-offset);

let co1;
let co2;

let co1in = 300 - co1;
let co2in = 300 - co2;



function setup() {
  createCanvas(windowWidth, windowWidth*0.75);
  frameRate(fr);
  pixelDensity(1);

  capture = createCapture(VIDEO);
  capture.hide();

  oscillator1(co1in);
  oscillator2(co2in);
}


function oscillator1(co1) {
  filter1 = new p5.BandPass();
  filter1.freq(co1);
  filter1.res(30);

  osc1 = new p5.Oscillator();
  osc1.disconnect();
  osc1.connect(filter1);
  osc1.setType('sawtooth');
  osc1.start();
  osc1.amp(0.3, 0.5);
  osc1.freq(120);
  osc1.pan(-0.8);
}


function oscillator2(co2) {
  filter2 = new p5.BandPass();
  filter2.freq(co2);
  filter2.res(30);

  osc2 = new p5.Oscillator();
  osc2.disconnect();
  osc2.connect(filter2);
  osc2.setType('sawtooth');
  osc2.start();
  osc2.amp(0.3, 0.5);
  osc2.freq(180);
  osc2.pan(0.8);
}


function draw() {
  translate(width,0);
  scale(-1.0,1.0);
  image(capture, 0, 0, 1200, 800);
  // filter("BLUR, 3");

  capture.loadPixels();
  loadPixels();

  // The two ellipses indicating the interaction points.
  fill(245, 50);
  strokeWeight(2);
  stroke(245, 80);
  ellipse(offset, offset, 125, 125);
  ellipse(1200-offset, offset, 125, 125);

  // Leftside square.
  push();
  strokeWeight(10);
  stroke(60, 30, 200);
  fill(45, 15, 140);
  rect(1200, 0, windowWidth-1200, 795);
  pop();

  co1 = pixels[pixelosc1]*5;
  co2 = pixels[pixelosc2-500]*5;

  filter1.freq(co1);
  filter2.freq(co2);

  console.log(co1);
  console.log(co2);
}
