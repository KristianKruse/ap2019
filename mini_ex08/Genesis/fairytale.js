
// Main character
let randomCharacter = [
  " Great Wizard ",
  " Queen ",
  " King ",
  " Warlock ",
  " Elf ",
  " Evil Witch ",
  " Wolf ",
  " Mysterious Creature ",
  " Three Little Pigs "
];

// Random Places
let randomPlace1 = [
  " Creepy Castle ",
  " Stinking Swamp ",
  " Dark Crypt ",
];

let randomPlace2 = [
  " High Tower ",
  " Haunted House ",
  " Lovely Woods ",
];

let randomPlace3 = [
  " Mysterious Maze ",
  " Dark Forrest ",
];

// Random Objects
let randomObject = [
  " Mythical Creature ",
  " Fairy Godmother ",
  " Dragon ",
  " Leaky Couldron ",
  " Heart ",
  " Rose with Thorns ",
  " Mermaid ",
  " Crystal Ball ",
  " Rainbow Dust ",
];

let randomObjects = [
  " Unicorns ",
  " Magical Wands ",
  " Talking Animals ",
  " Houses made of Candy ",
  " Shoes made of Glass ",
  " Breadcrumbs ",
  " Pots of Gold ",
];
