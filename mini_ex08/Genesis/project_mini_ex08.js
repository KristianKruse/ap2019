let castleImg;
let spacing = 32;

// let _God;
// let heavens, earth;
// let light;


function preload() {
  img = loadImage("fairytaleImage.jpg");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(10);
  image(img, 0, 0);

  fill(random(150,255), random(150,255), random(150,255));
  strokeWeight(5);
  stroke(10);
  textSize(int(windowWidth/80));
  textAlign(CENTER);

  // Characters:
  _God = randomCharacter[int(random(10))];

  // Places:
  heavens = randomPlace1[int(random(3))];
  earth = randomPlace2[int(random(3))];
  water = randomPlace3[int(random(2))];


  // // Objects:
  vault = randomObject[int(random(4))];
  light = randomObjects[int(random(4))];
  darkness = randomObject[int(random(4, 9))];
  dryGround = randomObjects[int(random(4, 7))];
}

function draw() {
  push();
  textSize(int(windowWidth/40));
  text('Refresh for a new exiting fairytale!', windowWidth/2, height*0.8);
  pop();

  // The book of genesis - first chapter
  text('In the beginning the' + _God + 'created the' + heavens + 'and the' + earth + '.', windowWidth/2, height/4);
  text('Now the' + earth + 'was formless and empty,' + darkness + 'was over the surface of the deep, and the Spirit of the' + _God + 'was hovering over the' + water + '.', windowWidth/2, height/4+spacing);
  text('And the' + _God + 'said, “Let there be' + light + '” and there was' + light + '.', windowWidth/2, height/4+spacing*2);
  text('The' + _God + 'saw that the' + light + 'was good, and the' + _God + 'separated the' + light + 'from the' + darkness +'.', windowWidth/2, height/4+spacing*3);
  text('The' + _God + 'called the' + light + '“day,” and the' + darkness + 'he called “night.” And there was evening, and there was morning—the first day.', windowWidth/2, height/4+spacing*4);
  text('And the' + _God + 'said, “Let there be a' + vault + 'between the' + water + 'to separate' + water + 'from' + water + '.”', windowWidth/2, height/4+spacing*5);
  text('And the' + _God + ' said, “So the' + _God + 'made the' + vault + 'and separated the' + water + 'under the' + vault + 'from the' + water + 'above it.', windowWidth/2, height/4+spacing*6);
  text('And it was so. The' + _God + 'called the' + vault + '“sky.” And there was evening, and there was morning—the second day.', windowWidth/2, height/4+spacing*7);
  text('And the' + _God + 'said, “Let the' + water + 'under the sky be gathered to one place, and let' + dryGround + 'appear.”', windowWidth/2, height/4+spacing*8);
  text('And it was so. The' + _God + 'called the' + dryGround + '“land,” and the gathered' + water + 'he called “seas.”', windowWidth/2, height/4+spacing*9);
  text('And the' + _God + ' saw that it was good.', windowWidth/2, height/4+spacing*10);
}
