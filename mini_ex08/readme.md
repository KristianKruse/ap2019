https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex08/Genesis/index.html

![](/mini_ex08/Images/Skærmbillede%202019-04-02%20kl.%2016.59.15.png)

This project has been made by Emily and me, and our idea is to create something entertaining by mixing two textual universes. Early in the process we chose the first chapter of bible (the book of genesis) as one of the texts. Then we wondered if it was possible to mix this text with some well known moderen pop song topping the charts, but having to  take the amount of syllables and word classes into account seemed a bit too difficult, so we ended up going for another solution. We found and created some lists containing words relating to fairytales. Then our idea was to randomly change some of the elements and character in the text to these fairytale figures. This you can see in the part in the bottom half section of our main code shown below. So our program ended up as a random fairytale generator, that, by taking advantaged of the bible as structure, is critiquing the biblical stories as being mere fairytales. For the visual part of this project, we wanted something that look quite ridiculous, so we found a corny wallpaper, with a fairytale theme, and also randomized the text color to follow the color scheme.

![](/mini_ex08/Images/Skærmbillede%202019-04-02%20kl.%2017.01.28.png)

When you look at the code, you might see that we haven’t been using a JSON file. After struggling with making the JSON file work, we end up simply using an array, which also seems just as easy for this specific project. We also tried to express our programs intention by setting variables like: 

// Characters:
  _God = randomCharacter[int(random(10))];

