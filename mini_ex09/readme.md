Mini Exercise 09
Group 4: Emily Knutsson, Kristian Kruse, Aia Kragh, Cecilie Frandsen
Title: function colorPic(k)
source of API: https://pixabay.com
data field: images (jpg.files) / largeImageURL
RunMe
SourceCode

What is the program about:
The program “function colorPic(k)” consists of two elements; an image and a color palette. When a color is clicked, the program will show an image related to that color. The image is generated with a web API from pixabay.com.
The concept in the program explores how classification has a huge impact on digital data.
In the program we try to explore how the digital media is seen as only visual represented (hereby we mean with no text visible) whereas the code behind it is only based on text. Color is one of the things in the real world we have the same approach to as in the digital world.
We as people all have the same understanding of the name of the color (like blue), but we might have different understandings of what that color actually looks like, but never have a way to confirm it.
We can tell when people aren’t seeing color the “right” way, for example we know when someone is colorblind, but we still can’t confirm that the nuances of the color blue is the same to one person as the person next to them.
The color turquoise is a good example of a color where people are often disagreeing on whether it is more blue or green. Besides that, people also different associations to a color.
The association also depend on the culture. In asia the color orange/yellow is a religious color where in western culture it is often used for sale in stores.
With the program we hope to make people think about what they associate with the different colors - and whether the program succeeds in matching the color with the expectations of the viewer, i.e green = nature or green = hulk?
Describe and reflect your process of making this mini exercise in terms of acquiring, processing, using and representing data?
In our first conceptual idea, we wanted to use two different API’s to give different results on the same search word. The concept of this was to explore how we classify certain data at websites. We mostly have a website for one kind of media at a time. Spotify = music. Soundcloud = sound/music. Pinterest = pictures. Google images = images. Youtube = video. It is rare that we have a website where we are presented with a mix of media, especially visual and audio.
To find these two API’s we split up our group into two teams looking for an API we could make work. A few hours later, we realized how difficult it was to make the idea come to reality and switched up our strategy. We decided to try to just find one API that we could make and we would then base our concept on which API worked (although we did try to look specifically for image API’s).
We got an API to work and then developed our concept. We then split up the team again and worked on code and readme separately.
When we started out this exercise, we were too ambitious in our ability to use (and understand) APIs, which ended up setting us back in our progress and nearly starting over. We had even talked before beginning about not being too ambitious considering the API but forgot to consider our ability to actually work with it.
As soon as we had a working API, we were able to conceptualize an idea and finally work further on the code and the aesthetics of the program.
How much do you understand this provided data? How do platform providers sort the data and give you the selected data? What are the power-relationship in the chosen APIs?
We have a fairly good understanding of the data in our API. We know it is full of images and videos, and that these can be called upon through search words (q) or certain tags. We also know that we can only call upon a specific amount of images per search (500).
Unfortunately, we don’t know how it specifically ties a word to an image only that the image is provided. We can see that the tags aren’t specifically tied to the search word so it is not (or at least not only) a tag based search.
link to the different parameters that can be used in the API

Figure 1: screenshot of Web API / JSON data
When we look at figure 1, we see an overview of the data contained in the API.

Figure 2: some of the API parameters
When using the color parameter the colors generally appear more dominant in the picture - so when using the q-parameter = blue and the colors-parameter = red. The image-file data which appear are mostly red with a blue item or part as well. See figure 3.

Figure 3: Photo from pixabay with search word blue and color parameter as red.
In our program we use the q=color and the colors parameter is chosen via the buttons.
What's the significance of APIs in digital culture? (If possible, please make an explicit linkage with the assigned reading):
APIs have become increasingly significant in digital culture since their implementation in digital culture. APIs makes it possible to share and use data across different websites, which is the base of social media culture. An example could be social media platforms where likes become an economy because it can be seen as data/number in a database. In our program we know that the default order of the pictures is popular. In this case likes are not solely the reason
for popularity but contributes to it as well as downloads, favourites, comments and views. All these are defined by numbers which does not tell anything about the positivity of the interaction. So popularity in this system is based on the amount of interaction with the picture - but interaction can be provoked both negatively and positively. The fact that there is no differentiation in what contributes to popularity has an impact on what is shown when we use databases and API-data.
As can be read from the quote below the APIs make it possible for the viewers/users of the internet to have an impact on what is shown and contribute to an always shifting system - but as it is argued the system is not necessarily neutral or objective.

"As abstract interfaces that facilitate the spread of these aggregate interfaces that we now give the name of platform to, APIs in these instances can be seen as making available and extensible the parts of a particular system so as to turn this system into a more expandable, dynamic, but also oftentimes hegemonic or asymmetrically aligned “ecosystem”." (Snodgrass & Soon, 2019, website article, https://firstmonday.org/ojs/index.php/fm/article/view/9553/7721)

Try to formulate a question in relation to web APIs or querying processes that you want to investigate further when you have more time.

How is a search word related to the images shown if it is not based solely on tags?
How is the data collected? And are there any rules regarding collecting and making apis public?

inspiration:
https://www.xrite.com/hue-test
test to test how precise your color vision is.

Color palette inspiration for the program aesthetics. (Pinterest)

Example of how color is used to categorize media in already existing websites (Google Images).