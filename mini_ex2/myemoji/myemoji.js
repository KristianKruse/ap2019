// Variables controlling mouth position
let x1 = 0;
let y1 = 0;
let x2 = 0;
let y2 = 0;

// Variables controlling left eye and eyebrow position
let a1 = 0;
let b1 = 0;
let a2 = 0;
let b2 = 0;

// Variables controlling right eye and eyebrow position
let c1 = 0;
let d1 = 0;
let c2 = 0;
let d2 = 0;

// Randomizing features when clicking the mouse
function mousePressed() {
  x1 = random(100)
  y1 = random(100)
  x2 = random(100)
  y2 = random(100)

  a1 = random(30)
  b1 = random(30)
  a2 = random(100)
  b2 = random(100)

  c1 = random(30)
  d1 = random(30)
  c2 = random(100)
  d2 = random(100)
}

function setup() {
  createCanvas(880, 800);
}


function draw() {
  background(250);
  noFill();
  strokeWeight(20);

// Text in top left corner
  push()
  fill(0,0,0);
  textSize(16);
  text("Press mouse to change expression\nPress S to save",20,20);
  pop()

// The mouth
  curve(mouseX, mouseY, x1 + 220 + mouseY / 20, y1 + 500, x2 + 580 - mouseY / 30, y2 + 500, mouseX, mouseY);

// The eyebrows
  curve(mouseX / 2, mouseY / 2, a1 + 180 + mouseX / 30, b1 + 160, a2 + 260, b2 + 130 + mouseY / 30, mouseX / 2, mouseY / 2);
  curve(mouseX / 2, mouseY / 2, 540 - c1, d1 + 160 + mouseY / 20, 620 + c2, d2 + 130 + mouseX / 30, mouseX / 2, mouseY / 2);

// The eyes
  ellipse(231 + a1 + mouseY / 20, 250 + b1 + mouseX / 20, a2 + mouseX / 20 - 15, b2 + mouseY / 20 - 15);
  ellipse(580 - c1 + mouseY / 20, 250 + d1 + mouseX / 20, c2 + mouseX / 20 - 15, d2 + mouseY / 20 - 15);
}

// Saving the emoji
function keyTyped(){
  if (key === "s"){
    saveCanvas("myemoji");
  }
}
