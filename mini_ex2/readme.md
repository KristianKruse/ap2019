I have chosen to make a simple solution for crafting your own emojies. It consists of three basic elements: a randomizition of the placement of the facial features when the mouse is clicked; a change of expression when the mouse is moved; and the option to to save your work by pressing the "s" key.  

Link: https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex2/myemoji/index.html


![](Images%20-%20myemoji/myemoji-10.png)


![](Images%20-%20myemoji/myemoji-11.png)

These two images are showing two possible expressions made within the same placement of the facial features. This means that the movement of the mouse is what makes it change from the first to the second image. 
I think that the original use of emojies, first and foremost, have been for avoiding any misunderstandings when messaging. The tradional and simple way of dooing this is by combining signs such as: ;), :), :D etc. Although this might seem a bit basic nowadays, I believe that the minimalistic approach is a great solution for designing new emojies. It is a way of cutting to the chase and using emojies as an emotional expression, that is adding colour to your writing, and not as a detailed statement in of itself. When you choose to remove every redundant part of the emojies, such as skin colour and gender, you also removes the parts that are separating people. The focus is instead shifted towards the things we have in common. 
I have tried to create an emoji that are very simplistic, but yet customizable. I'm imagining that my solution could be used as an add-on to something like Messenger, and be a fast way for people to express their emotions when texting, in an unbiased, but yet personal way. Hopefully it would also be a enjoyable and fun experience. 


![](Images%20-%20myemoji/Skærmbillede%202019-02-19%20kl.%2016.34.50.png)


![](Images%20-%20myemoji/Skærmbillede%202019-02-19%20kl.%2016.35.12.png)

As can be seen in the images of my code above, I have been using the random() and mouseX/Y functions alot. The random() are setting the value af the variables controlling the general placement of the facial features, and the mouseX/Y variables are controlling their relative position when the mouse is being moved. There is quite a bit of repetition in my code (ex. setting the varialbes), and I wonder wether it is possible to do this in a more effincient way, maybe with an array or loop. Another place in the code that I would like to shorten is the curve()s. Their parameters seem a bit too complex, and i guess these parameters somehow could have been written as variables set outside the functions. 