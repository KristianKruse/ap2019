https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/MiniEx1/p5/empty-example/index.html



Figuring out how Gitlab and the git client GitKraken works, has been quite a task for me. It took much longer than I anticipated, but hopefully, everything is now in its right place.   

In this particular program, I'm testing some of the ideas described in the videos made by Daniel Schiffman and the Coding Train on YouTube. The program is built around the concept of having a class, that describes the single object, and the functions running the animation.
So far, my learning proces consisted of watching tutorials and doing the coding presented in the videos. This has already given me a huge understanding of some of the basic elements of JS, and eventhough it might not be something i fully understand as of now, it has contributed to creating an intuition of what to reference when facing challenges. 