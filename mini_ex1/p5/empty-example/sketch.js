// MiniEx1 - Kristian Kruse 11.2.2019
// Inpired by Daniel Shiffman

let planet = [];

function setup() {
  createCanvas(1440, 800);
}

// the code for spawning the cirkles

function mouseDragged() {
  let r = random(30, 100);
  let b = new Planet(mouseX, mouseY, r);
  planet.push(b);
}

// the code for animating the cirkles

function draw() {
  background(15, 15, 30);
  for (let i = 0; i < planet.length; i++) {
    planet[i].move();
    planet[i].show();
  }
}


// the individual blinking circles

class Planet {
  constructor(x,y,r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }

  move() {
    this.x = this.x + 1;
    this.y = this.y + 1;
  }

  show() {
    stroke(255);
    strokeWeight(4);
    fill(200, 255, random(0, 255));
    ellipse(this.x, this.y, this.r * 2);
  }
}
