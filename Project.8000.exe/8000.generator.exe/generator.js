// Possible camera settings
let iso = [100,200,400,800,1600,3200,6400];
let aperture18 = [3.5,4.0,4.5,5.0,5.6,6.3,7.1,8.0,9.0,10,11,13,14,16,18,20,22]; // depending on focal length
let aperture24 = [4.0,4.5,5.0,5.6,6.3,7.1,8.0,9.0,10,11,13,14,16,18,20,22,25]; // depending on focal length
let aperture35 = [4.5,5.0,5.6,6.3,7.1,8.0,9.0,10,11,13,14,16,18,20,22,25,29]; // depending on focal length
let aperture55 = [5.6,6.3,7.1,8.0,9.0,10,11,13,14,16,18,20,22,25,29,32,36]; // depending on focal length
let shutterFlash = [4,5,6,8,10,13,15,20,25,30,40,50,60,80,100,125,160,200]; // depending on focal length
let shutterFlashOff = [4,5,6,8,10,13,15,20,25,30,40,50,60,80,100,125,160,200,250,320,400,500,640,800,1000,1250,1600,2000,2500,3200,4000]; //afhængig af aperture
let flash = ['On','Off'];

// Time and situation
let time = ['8-10','12-14','16-18','20-22','00-02','04-06'];
let perspective = ['Up','Down','Neutral'];
let direction = ['East','West','North','South'];
let action = ['Closed eyes','Jump','No Action','Arms stretched','Sit down','Lie down','No action','One leg'];

// Text setup
let spacing = 30;


function setup() {
  frameRate(1);
  createCanvas(windowWidth, windowHeight);
  background(62,64,68);
}

function draw() {
  button();
}

// Saving the canvas
function keyTyped() {
  if (key === "s") {
    saveCanvas("Instructions");
  }
}

function button() {
  Rules = createButton('Generate Instructions');
  Rules.position(50, 30);
  Rules.mousePressed(generateRules);

}


// The primary function:
function generateRules() {
  textSize(20);
  background(62,64,68);
  fill(0,255,0);

  // Generating time and place
  textStyle(BOLD);
  text('Time and place:',50,spacing*3)
  textStyle(NORMAL);

  text('Location Coordinates: ' + random(56.139657, 56.176508).toFixed(6) + ', ' + random(10.181802, 10.231154).toFixed(6), 50, spacing*4);
  text('Time of Day: ' + random(time), 50, spacing*5);

  // Generating camera settings
  textStyle(BOLD);
  text('Camera settings: ', 50, spacing*7);
  textStyle(NORMAL);

  text('ISO: ' + random(iso), 50, spacing*8);

  // Aperture is depending on the focal length (indeks 0-20)
  if (frameCount%4 === 0) {
    text('Focal Length: ' + 18, 50, spacing*10);
    text('Aperture: ' + random(aperture18), 50, spacing*11);
  } else if (frameCount%4 === 1) {
    text('Focal Length: ' + 24, 50, spacing*10);
    text('Aperture: ' + random(aperture24), 50, spacing*11);
  } else if (frameCount%4 === 2) {
    text('Focal Length: ' + 35, 50, spacing*10);
    text('Aperture: ' + random(aperture35), 50, spacing*11);
  } else if (frameCount%4 === 3){
    text('Focal Length: ' + 55, 50, spacing*10);
    text('Aperture: ' + random(aperture55), 50, spacing*11);
  }

  // Shutter is depending on the blitz
  if (frameCount%2 === 0) {
    text('Flash: ' + flash[0], 50, spacing*9);
    text('Shutter: ' + random(shutterFlash), 50, spacing*12);
  } else if (frameCount%2 === 1) {
    text('Flash: ' + flash[1], 50, spacing*9);
    text('Shutter: ' + random(shutterFlashOff), 50, spacing*12);
  }

  // Generating situational rules
  textStyle(BOLD);
  text('Situation:', 50, spacing*14);
  textStyle(NORMAL);

  text('Perspective: ' + random(perspective), 50, spacing*15);
  text('Direction: ' + random(direction), 50, spacing*16);
  text('Action: ' + random(action), 50, spacing*17);

  text('Press the "S"-key to save your instructions', 50, spacing*19);
  textSize(16);
  text('When the photo has been taken, email it and it’s corresponding instructions to:', 50, spacing*23);
  text('8000.human.exe@gmail.com', 50, spacing*24);

}
