// Class variables:
let dotSize = 8;
let dotGrow = 16;
let showPhoto = false;
let spacing = 18;


class Dot {
  constructor(x, y, r, photo, instruction) {
    this.x = x;
    this.y = y;
    this.r = r;

    this.photo = photo;
    this.instruction = instruction;
  }

  contains(px, py) {
    let d = dist(px, py, this.x, this.y);
    return (d < this.r*1.3);
  }

  changeSize() {
    this.r = dotGrow;
  }

  reSize() {
    this.r = dotSize;
  }

  showPhoto() {
    image(this.photo, windowWidth/50, windowHeight/30, windowWidth/2, windowHeight/2);
  }

  show() {
    stroke(0,random(200, 255),0);
    strokeWeight(3);
    fill(255);
    circle(this.x, this.y, this.r);
  }

  showInstructions() {
    let textPlacement = windowWidth/50;
    textSize(14);
    stroke(0,random(50,100),0)
    strokeWeight(1);
    fill(0,255,0);

    push();
    translate(0, windowHeight/2);

    // Showing time and place
    textStyle(BOLD);
    text('Time and Place:', textPlacement, spacing*3)
    textStyle(NORMAL);

    text('Location Coordinates: ' + this.instruction[0] + ', ' + this.instruction[1], textPlacement, spacing*4);
    text('Time of Day: ' + this.instruction[2], textPlacement, spacing*5);

    // Showing camera settings
    textStyle(BOLD);
    text('Camera Settings: ', textPlacement, spacing*7);
    textStyle(NORMAL);

    text('ISO: ' + this.instruction[3], textPlacement, spacing*8);
    text('Focal Length: ' + this.instruction[5], textPlacement, spacing*9);
    text('Aperture: ' + this.instruction[6], textPlacement, spacing*10);
    text('Flash: ' + this.instruction[4], textPlacement, spacing*11);
    text('Shutter: ' + this.instruction[7], textPlacement, spacing*12);

    // Showing situational rules
    textStyle(BOLD);
    text('Situation:', textPlacement, spacing*14);
    textStyle(NORMAL);

    text('Perspective: ' + this.instruction[8], textPlacement, spacing*15);
    text('Direction: ' + this.instruction[9], textPlacement, spacing*16);
    text('Action: ' + this.instruction[10], textPlacement, spacing*17);
    pop();
  }
}
