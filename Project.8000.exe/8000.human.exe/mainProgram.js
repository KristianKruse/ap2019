// General variables
let fr = 10;
let numDots = 21;

// For translating the coordinates to x and y positions
let cXmin = 10.181802;
let cYmin = 56.139657;
let cXdiff = 0.049352;
let cYdiff = 0.036851;

// Arrays
let dots = [];
let photos = [];


function preload() {
  for (let i = 0; i < numDots; i++) {
    photos[i] = loadImage('photos/photo' + i + '.JPG')
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(fr);

  // Link to the generator program
  let link = createA('https://cdn.staticaly.com/gl/KristianKruse/ap2019/master/Project.8000.exe/8000.generator.exe/index.html', 'generator', '[_top]');
  link.style('color', '#0cff00');
  link.style('font-size', '20px');
  link.position(windowWidth*0.9, windowHeight/20);

  // For conversion of coordinates
  let wX = windowWidth*0.45;
  let wY = windowHeight*0.9;
  let marginX = windowWidth*0.538;
  let marginY = windowHeight*0.05;

  // Creating the individual dots with specific values
  for (let i = 0; i < numDots; i++) {
    let photo = photos[i];
    let instruction = instructions[i];
    let x = ((instructions[i][1]-cXmin)/cXdiff) * wX + marginX;
    let y = ((1-(instructions[i][0]-cYmin)/cYdiff)) * wY + marginY;
    dots[i] = new Dot(x, y, dotSize, photo, instruction);
  }
}

function draw() {
  background(62,64,68);
  textStyle(BOLD);
  textSize(36);
  fill(0,255,0);
  text('8000.human.exe', windowWidth/50, windowHeight/15);
  textStyle(NORMAL);

  for (let i = 0; i < dots.length; i++) {
    // If the mouse-pointer is inside a dot:
    if (dots[i].contains(mouseX, mouseY)) {
      dots[i].changeSize();
      dots[i].showPhoto();
      dots[i].showInstructions();
    } else {
      dots[i].reSize();
    }
    dots[i].show();
  }
}



// Lists of the instructions created by the generator-program, that were used by us when taking each specific Picture
let instructions = [
  [56.155388,10.192739,'16-18',6400,'Off',24,18,40,'Neutral','South','Arms stretched'],
  [56.157124,10.206025,'16-18',400,'On',35,9.0,125,'Up','West','Arms stretched'],
  [56.169713,10.182234,'16-18',1600,'On',18,16,100,'Down','East','Jump'],
  [56.150227,10.196785,'16-18',200,'Off',24,25,125,'Down','North','No action'],
  [56.141956,10.198440,'16-18',800,'On',18,4.5,5,'Up','South','Lie down'],
  [56.146190,10.191985,'20-22',6400,'On',18,8,13,'Neutral','East','Closed eyes'],
  [56.142223,10.194058,'20-22',400,'Off',55,10,15,'Up','South','Closed eyes'],
  [56.140295,10.182305,'20-22',100,'On', 35,29,25,'Down','East','Closed eyes'],
  [56.162426,10.229220,'8-10',200,'On',18,11,125,'Neutral', 'North','Arms stretched'],
  [56.160800,10.225333,'8-10',6400,'Off',55,13,640,'Neutral','North','Jump'],
  [56.141103,10.210309,'8-10',200,'Off',55,16,50,'Neutral','South','Sit down'],
  [56.153132,10.204765,'00-02',6400,'On',35,5.6,160,'Down','West','Jump'],
  [56.161664,10.203159,'00-02',6400,'Off',24,22,400,'Down','East','No action'],
  [56.153234,10.187604,'12-14',1600,'On',18,20,50,'Down','North','No action'],
  [56.149235,10.185042,'12-14',1600,'Off',55,32,800,'Up','North','Sit down'],
  [56.175974,10.206330,'12-14', 1600,'Off',24,16,5,'Neutral','East','No action'],
  [56.154847,10.226639,'16-18', 3200,'Off',24,14,80,'Neutral','North','Sit down'],
  [56.145328,10.228540,'16-18', 200,'On',18,9,125,'Neutral','East','One leg'],
  [56.154026,10.183239,'00-02',800,'Off',55,14,15,'Down','East','Closed eyes'],
  [56.161108,10.189590,'00-02',6400,'Off',24,22,200,'Up','North','Arms stretched'],
  [56.144955,10.183125,'00-02',400,'Off',55,29,50,'Neutral','North','Arms stretched']
];
