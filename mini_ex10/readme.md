[runme for mini_ex07](https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex07/Final%20version/index.html)
Remember to switch on sound in browser settings

[Link to the mini_07 folder](https://gitlab.com/KristianKruse/ap2019/tree/master/mini_ex07)

[Link to the flowchart for mini_ex07](https://gitlab.com/KristianKruse/ap2019/blob/master/mini_ex10/Flowchart%20mini_ex10.pdf)

The flowchart for this programme ended up becoming way more complex than I anticipated, and I think it shows that it doesn’t make sense to do a flowchart this detailed, when the programme exceeds some kind of threshold of complexity. I originally wanted to have every part of the programme represented in the flowchart like it was a common language that could be translated into every other coding language, but since I realized how complex the flowchart then had to be, I did let out some details. 

A thing I’ve thought about when doing a flowchart like this, is that it seems nearly the same as a visual coding language such as Max/MSP. I have been using that a bit, and some people, I guess, would find it easier that working with regular code - as long as it’s set up in a structured layout, probably containing layers. 




![](https://gitlab.com/Kragh/ap2019/raw/master/miniExes/MiniEx10/fc1.png)

This is a flowchart outlining the basics of our first idea which involves showing two descriptions of two people. The user is then told to select between them, and when that happens the faces of both if them are shown - the one that has been chosen and the one that has been discarded. The idea is to challenge the users prejudices based on visual appearance.  

This isn’t the most technical challenging idea, but maybe it can take some time making the JSON file of the pictures and the persons work. The most challenging about this idea is probably to find people who want to participate and let us take their picture for something they’re not really able to control. 




![](https://gitlab.com/Kragh/ap2019/raw/master/miniExes/MiniEx10/fc2.png)

The second flowchart shows a programme idea based on a 3D environment creating a map of interconnected dots, which the user can click on. When that happens, the user is take to a footage and an audio clip of some place in the city of Aarhus. The user can continue to click around the connected dots to experience new parts of the city. 

Our goal is to show new ways of experiencing the city and not to make a geographically true map of the places. It focuses on the memory, feelings and relations of experiencing the world instead of being a functional maps for finding your way. It can also be seen as a place for finding inspiration to go out and experience the city in new ways. 

I think the most challenging about this program will be to make a well functioning and good looking 3D environment interact with rather large datasets of both pictures and audiofiles. 

The big difference between the group flowcharts and my own is the amount of details. The group ones could be seen as being on the state of the vision or operative image, while my own is full of specifications. 