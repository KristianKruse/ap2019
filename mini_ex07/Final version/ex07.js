// Visual and temporal parameters
let size = 50;
let growth = 5;
let fr = 26;
let mult = 16;

// The following is the setup for the four different voices.
// Bass setup
let oscBass, envBass, envBassFilter, bassFilter;
let bassPlaying = false;
let rythmBassAmp = 3*mult;// Rythm for opening the volume of the amp and the X-axis size of the ellipsoid.
let rythmBassFilter = 5*mult;// Rythm for opening the filter and the Y-axis size of the ellipsoid. 

// Mid1 setup
let oscMid1, oscMid12, envMid1, envMid1Filter, mid1Filter;
let mid1Playing = false;
let rythmMid1Amp = 5*mult;
let rythmMid1Filter = 7*mult;

// Mid2 setup
let oscMid2, oscMid22, envMid2, envMid2Filter, mid2Filter;
let mid2Playing = false;
let rythmMid2Amp = 7*mult;
let rythmMid2Filter = 11*mult;

// High setup
let oscHigh, envHigh, envHighFilter, highFilter;
let highPlaying = false;
let rythmHighAmp = 2*mult;
let rythmHighFilter = 3*mult;



function setup() {
  frameRate(fr);
  createCanvas(windowWidth, windowHeight, WEBGL); // 3D environment
  directionalLight(255, 255, 255, 10000, 0, -10000); // 3D directional light
  ambientLight(30); // 3D ambient light

  // Calling my four voice-functions
  bass();
  mid1();
  mid2();
  high();

  // Adding reverb to the two mid voices
  reverb = new p5.Reverb();
  mid1Filter.connect(reverb);
  mid2Filter.connect(reverb);

  // Adding compression on the master signal
  compress = new p5.Compressor();
  reverb.connect(compress);
  bassFilter.connect(compress);
  highFilter.connect(compress);
}


function draw() {
  background(10);

  // Drawing the ellipsoid for the Bass and High voices
  push();
  ambientMaterial(200+frameCount%rythmHighAmp*growth, 100+frameCount%rythmHighFilter*growth, 50+frameCount%rythmHighFilter*growth);
  translate(0, 0, -500);
  ellipsoid(size*2 + frameCount%rythmBassAmp*growth, size*2 + frameCount%rythmBassFilter*growth, size*2 + 0, 50, 50);
  pop();

  // Drawing the ellipsoid for the mid1 voice
  push();
  ambientMaterial(50, 200, 100);
  translate(-windowWidth/2, 0, -500);
  ellipsoid(size + frameCount%rythmMid1Amp*growth, size + frameCount%rythmMid1Filter*growth, size + 0, 50, 50);
  pop();

  // Drawing the ellipsoid for the mid2 voice
  push();
  ambientMaterial(100, 50, 200);
  translate(windowWidth/2, 0, -500);
  ellipsoid(size + frameCount%rythmMid2Amp*growth, size + frameCount%rythmMid2Filter*growth, size + 0, 50, 50);
  pop();


  // Telling the different voices when they should play.
  // Bass
  if (frameCount%rythmBassAmp === 1) {
    envBass.play();
  }
  if (frameCount%rythmBassFilter === 1) {
    envBassFilter.play();
  }

  // Mid1
  if (frameCount%rythmMid1Amp === 1) {
    envMid1.play();
  }
  if (frameCount%rythmMid1Filter === 1) {
    envMid1Filter.play();
  }

  // Mid2
  if (frameCount%rythmMid2Amp === 1) {
    envMid2.play();
  }
  if (frameCount%rythmMid2Filter === 1) {
    envMid2Filter.play();
  }

  // High
  if (frameCount%rythmHighAmp === 1) {
    envHigh.play();
  }
  if (frameCount%rythmHighFilter === 1) {
    envHighFilter.play();
  }
}


// All my funtions for the voices
function bass() {
  // Envelope for when the sound should play
  envBass = new p5.Envelope();
  envBass.setADSR(0.1, 0.3, 0.5, 0.5);
  envBass.setRange(1.5, 0);

  // Envelope for when the filter should open
  envBassFilter = new p5.Envelope();
  envBassFilter.setADSR(0.1, 0.4, 0.5, 0.3);
  envBassFilter.setRange(400, 80);

  bassFilter = new p5.LowPass();

  // Creating the sound oscillator
  oscBass = new p5.Oscillator();
  oscBass.setType('triangle');
  oscBass.freq(41.2); // The frequency of an low e-note. This is the lowest note on a traditional tuned e-bass.
  oscBass.disconnect();
  oscBass.connect(bassFilter); // Connecting to the filter.
  oscBass.amp(envBass); // Connecting the envelope for controlling the amplitude of the sound.
  oscBass.start();// Starting the oscillator.
  bassFilter.res(3);// Setting the resonance of the filter.
  bassFilter.freq(envBassFilter); // Connecting the envelope for controlling the cutoff frequency of the sound filtering.
}


function mid1() {
  envMid1 = new p5.Envelope();
  envMid1.setADSR(0.1, 0.3, 0.5, 1.1);
  envMid1.setRange(0.2, 0.0);

  envMid1Filter = new p5.Envelope();
  envMid1Filter.setADSR(0.03, 0.5, 0.5, 0.5);
  envMid1Filter.setRange(1500, 400);

  mid1Filter = new p5.LowPass();

  // In the two mid voices, I use dual oscillator for creating some more interesting harmonization.
  oscMid1 = new p5.Oscillator();
  oscMid1.setType('sawtooth');
  oscMid1.freq(246.94);
  oscMid1.amp(envMid1);
  oscMid1.start();
  oscMid1.disconnect();
  oscMid1.connect(mid1Filter);

  // Here's the second one.
  oscMid12 = new p5.Oscillator();
  oscMid12.setType('sawtooth');
  oscMid12.freq(554.37);
  oscMid12.amp(envMid1);
  oscMid12.start();
  oscMid12.disconnect();
  oscMid12.connect(mid1Filter);

  mid1Filter.res(5);
  mid1Filter.freq(envMid1Filter);
  oscMid1.pan(0.9);
}


function mid2() {
  envMid2 = new p5.Envelope();
  envMid2.setADSR(0.03, 0.3, 0.3, 0.6);
  envMid2.setRange(0.2, 0);

  envMid2Filter = new p5.Envelope();
  envMid2Filter.setADSR(0.5, 0.5, 0.5, 0.5);
  envMid2Filter.setRange(2000, 600);

  mid2Filter = new p5.LowPass();

  oscMid2 = new p5.Oscillator();
  oscMid2.setType('sawtooth');
  oscMid2.freq(369.99);
  oscMid2.amp(envMid2);
  oscMid2.start();
  oscMid2.disconnect();
  oscMid2.connect(mid2Filter);

  oscMid22 = new p5.Oscillator();
  oscMid22.setType('sawtooth');
  oscMid22.freq(698.46);
  oscMid22.amp(envMid2);
  oscMid22.start();
  oscMid22.disconnect();
  oscMid22.connect(mid2Filter);

  mid2Filter.res(5);
  mid2Filter.freq(envMid2Filter);
  oscMid2.pan(-0.9);
}

// The high voice ended up being the high part of the bass in the middle of the stereo field.
function high() {
  envHigh = new p5.Envelope();
  envHigh.setADSR(0.5, 0.3, 0.3, 0.6);
  envHigh.setRange(0.5, 0);

  envHighFilter = new p5.Envelope();
  envHighFilter.setADSR(0.01, 0.1, 0.5, 0.5);
  envHighFilter.setRange(3000, 50);

  highFilter = new p5.LowPass();

  oscHigh = new p5.Oscillator();
  oscHigh.setType('triangle');
  oscHigh.freq(82.4);
  oscHigh.amp(envHigh);
  oscHigh.disconnect();
  oscHigh.connect(highFilter);
  oscHigh.start();
  highFilter.res(5);
  highFilter.freq(envHighFilter);
}
