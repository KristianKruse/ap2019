// The 5 voices
function bass() {
  let osc, filter, env, pEnv;

  // Filter envelope controlling cutoff
  let attackLevel = 1.0;
  let releaseLevel = 0;

  let attackTime = 0.001;
  let decayTime = 0.2;
  let susPercent = 0.2;
  let releaseTime = 0.5;

  envBass = new p5.Envelope();
  filterBass = new p5.LowPass();
  oscBass = new p5.Oscillator();

  envBass.setADSR(attackTime, decayTime, susPercent, releaseTime);
  envBass.setRange(attackLevel, releaseLevel);
  oscBass.setType('square');
  oscBass.freq(50);
  oscBass.amp(0.3);
  oscBass.disconnect();
  oscBass.connect(filterBass);
  oscBass.start();
  filterBass.freq(envBass*200);
  filterBass.res(10);


  // Creating the mathing ellipsoid by translating audio amplitude and frequency, to
  // size and color.
  let ampSize = 1/mouseY;
  let xPos = -windowWidth/3;
  ellipsoidsBass = new Ellipsoid(ampSize, colR1, colG1, colB1, xPos);
}



function lead() {
  let ampSize = random();
  let colR = mouseX;
  let colG = mouseX;
  let colB = mouseX;
  let xPos = -windowWidth/6;
  ellipsoidsLead = new Ellipsoid(ampSize, colR, colG, colB, xPos);
}



function pad() {
  let ampSize = random();
  let colR = mouseX;
  let colG = mouseX;
  let colB = mouseX;
  let xPos = 0;
  ellipsoidsPad = new Ellipsoid(ampSize, colR, colG, colB, xPos);
}




function deepRythm() {
  let ampSize = random();
  let colR = mouseX;
  let colG = mouseX;
  let colB = mouseX;
  let xPos = windowWidth/6;
  ellipsoidsDeepRythm = new Ellipsoid(ampSize, colR, colG, colB, xPos);
}




function highRythm() {
  let ampSize = random();
  let colR = mouseX;
  let colG = mouseX;
  let colB = mouseX;
  let xPos = windowWidth/3;
  ellipsoidsHighRythm = new Ellipsoid(ampSize, colR, colG, colB, xPos);
}
