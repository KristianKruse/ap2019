// only an ellipse class
class Ellipsoid {
  constructor(ampSize, colR, colG, colB, xPos) {
    this.colR = colR;
    this.colG = colG;
    this.colB = colB;
    this.ampSize = ampSize;
    this.xPos = xPos;
  }

  show() {
    // Creating arays for dealying the colors in the Y-axis ellipsoids.
    let aSize = [];
    let r = [];
    let g = [];
    let b = [];

    // The newest value is put on top of the array, and the last on is removed.
    r.unshift(this.colR); r.splice(numY);
    g.unshift(this.colG); g.splice(numY);
    b.unshift(this.colB); b.splice(numY);
    aSize.unshift(this.xPos); aSize.splice(numY);

    // The loop that's creating the actual ellipsoids using the data of the arrays.
    for (let j = 0; j < numY; j++) {
      push();
      translate(this.xPos, j*distance, j*distanceZ);
      ambientMaterial(r[j], g[j], b[j]);
      noStroke();
      ellipsoid(100, 50, 65);
      pop();
    }
  }
}
