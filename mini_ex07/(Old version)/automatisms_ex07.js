// Declaring the 5 main visual objects. One array of ellipsoids for each sound.
let ellipsoidsBass;
let ellipsoidsBassLead;
let ellipsoidsPad;
let ellipsoidsDeepRythm;
let ellipsoidsHighRythm;


// When

// Visual variables
let numY = 20;
let distance = 50;
let distanceZ = -10;
let colR1, colG1, colB1;


function setup() {
  frameRate(30);
  createCanvas(windowWidth, windowHeight, WEBGL);
  background(10);
  directionalLight(255, 255, 255, 500, 200, -500); // 3D light.

  // Calling the 5 sounds.
  bass();
  lead();
  pad();
  deepRythm();
  highRythm();
}
//
// function playEnvBass() {
//   envBass.play();
// }


function draw() {
  background(10);

  let colR1 = mouseX;
  let colG1 = mouseY;
  let colB1 = mouseX;

  // Drawing the 5 visuals.
  ellipsoidsBass.show();
  ellipsoidsLead.show();
  ellipsoidsPad.show();
  ellipsoidsDeepRythm.show();
  ellipsoidsHighRythm.show();
}
