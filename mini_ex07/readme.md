https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex07/Final%20version/index.html

When opening the program make sure to allow the use of sound in the browser, and to get the full experience, a pair of headphones would be preferable. 

![](/mini_ex07/Images/Skærmbillede%202019-03-26%20kl.%2010.53.24.png)
![](/mini_ex07/Images/Skærmbillede%202019-03-26%20kl.%2010.53.37.png)

For this assignment, I started out by wanting to create an audiovisual generative program based on 4 rows of bubbles, that kept falling falling from the top of the screen to the bottom. These were supposed to be a direct visual representations of the sound being generated with volume/amplitude translated into size, and frequency translated into color. My idea of them as constantly falling from the top to the bottom of the screen, was to show how the sounds were changing over time - like an animated musical score. I used quite some time setting up this project with three files: One for my class, one for the functions, and one for my main program. When i got to the point of getting testing it, I couldn’t make i run properly. It got a bit too confusing, and after stalling for some time, I chose to start all over. I have included this unfinished code in the folder named (Old Version) if you would like to see it. 

The final program could be called a simplified version of my initial idea. The program is synthesizing audio and visuals in corresponding patterns. I using four different voices, created entirely from sound synthesis, and matching visuals of the colors and sizes of three bubbles. The two bubbles on the right and left, are placed in correspondence to the placement of the Mid1 and Mid2 voices in the stereo field (one panned left and one right). The one in the middle, is representing both the high voice and the bass (centered in the stereo field). 

I have been using prime numbers for controlling the voices and the visuals. The reason for this is that they are basically never going to play the same sequence since they have to run for very long time before the program repeats it self. In that way, I have tried to create something generative, without using the random() function. The numbers i have been using are: 2, 3, 5, 7, and 11. 
When that amp of each of the voices opens and closes, the corresponding ellipsoid widens or shrinks in the X-axis, and when the filter for the voices opens and close, the same happens just in the Y-axis. When you’re looking at the code, it might seem a little confusing, especially with the sound generation part. In the top, you will find the most essential parameters, and I have tried giving a little explanation in the code. Also, I have been using the frameCount and the modulus (%) a lot for telling the program when to play a sound. Ex: frameCount%11 === 1, is used for telling that a voice should be play every eleventh time. 

![](/mini_ex07/Images/Skærmbillede%202019-03-26%20kl.%2011.23.32.png)
*The first part of the code*