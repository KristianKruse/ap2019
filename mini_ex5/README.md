Link: https://cdn.staticaly.com/gl/KristianKruse/ap2019/raw/master/mini_ex5/Webcam_synthesis_2/index.html

If there's no sound when you're running the program, you need to go to your browser setting for "this website" and change the setting "autoplay" to allow all media.

![](Images/Skærmbillede%202019-03-13%20kl.%2011.01.32.png)
*The picture is of the older version of the program. At that time the program only had two voices indicated by the two white ellipses with the low alpha value. Since this new version has many voices all across the screen, I have chosen to delete these ellipses.*

This is a new version of my mini_ex4. In this version I have been using an object oriented approach instead of duplicating functions, which means that I have been able to create as many instances of voice-pixel-pairs as I wanted.

The idea is to translate video material to sound. In this version, I do this by creating 20 oscillator and filter pairs. The array of oscillators are starting at a frequency of 200 Hz, and then their frequency increase by a factor of 1.5^i for each incrementation. This also means that all the oscillators essentially consists of the musical circle of fifths. Now each of these oscillator are run through a bandpass filter, and this filters cutoff frequency is controlled by the pixel values from the webcam image. This means that the user is able to open and close the filters of each of the 20 voices, simply by moving their body (or anything else for that matter) in front of the camera. I use the pixel array and sample one of the pixel values af different spots all across the screen, therefor it can be described as a translation of a 2D plane from video into audio. 


![](Images/Skærmbillede%202019-03-14%20kl.%2015.25.40.png)

![](Images/Skærmbillede%202019-03-14%20kl.%2015.26.01.png)

![](Images/Skærmbillede%202019-03-14%20kl.%2015.26.14.png)