// v. 3 (using WEBGL and 3D objects)

let dots = [];
let num = 20;
let numdots = 300
let sphsize = 50;
let dotalpha = 1;
let dotsx = 50;
let dotsy = 5;
let dotsz = 6;
let fc = -1.15;


// This is the loading of images which I couldn't make work

// let eyeclosed;
// let eyeopen;

// function preload() {
//   eyeclosed = loadImage('eyeclosed.png');
//   eyeopen = loadImage('eyeopen.png');


function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  frameRate(24);
  for (let i = 0; i < numdots; i++) {
    push();
    dots[i] = new dot();
  }
}

function draw() {
  directionalLight(255, 255, 255, 1, 0.5, -1);
  background(255, 245, 235);

  fc = sin(frameCount/200);
  sphsize = sphsize*((cos(frameCount/50)/100)+1);
  dotsz = dotsz*(sin(frameCount+18)/3);
  dotsx = dotsx*(cos(frameCount+30)/3);

  for (let i = 0; i < numdots; i++) {
    push();
    dots[i].move(i*dotsx, i*dotsy, i*dotsz, fc*i);
    dots[i].display(i*dotalpha);
    pop();
  }
}

function dot () {
  this.x = 100;
  this.y = 0;
  this.z = 0;


  this.move = function(a, b, c, d) {
    let cir = 360/num*((frameCount-d)%num);
    rotateZ(radians(cir));
    rotateX(10);

    if (frameCount > 1000)  {
      translate(dotsx+= 0.3, dotsy+= 0.3, dotsz+= 0.3);
    } else {
      translate(this.x+a, this.y+b, this.z+c);
    }
  }

  this.display = function(a) {
    noStroke();
    ambientMaterial(0, 150, 255, 255 - a);
    sphere(sphsize, 40, 40);
  }
}





// function unfold () {
//
// }
//
// function sin cos etc



// if (dotsx === windowWidth*0.25 || windowWidth*0.75) {
//   dotsx = dotsx * -1;
// }
// if (dotsy === windowHeight*0.25 || windowHeight*0.75) {
//   dotsy = dotsy * -1;
// }
// if (dotsz === 0 || 400) {
//   dotsz = dotsz * -1;
// }




// v. 2 (adding parameters and changing the for-loop)

// // Behavior
// let num = 12; // Number of circles
// let layers = 30; // Nummer of inner layers
// let decr = 0.90; // Decrementation value
// let frate = 10;
//
// // The individual circles
// let posx = 250; // Position of the first circle
// let posy = 0;
// let size = 90; // Size of the first circle
// let offset = 60; // The amount of twisting the inner circles
//
// // Colors
// let r = 100;
// let g = 20;
// let b = 255;
// let str = 150;
// let stralpha = 50;
//
//
// function setup() {
//   createCanvas(windowWidth, windowHeight, WEBGL);
//   // background(10, 10, 255);
//   frameRate(frate);
// }
//
// function draw() {
//   fill(20, 20, 10, 80);
//   noStroke();
//   rect(0, 0, width, height);
//   drawThrobber(num, posx, posy, size, offset, r, g, b, str, stralpha);
// }
//
// function drawThrobber(num1, posx1, posy1, size1, offset1, r1, g1, b1, str1, stralpha1) {
//  //move things to the center    // 360/num >> degree of each ellipse' move ;frameCount%num >> get the remainder that indicates the movement of the ellipse
//
//   let cir = 360/num1*(frameCount%num1);  //to know which one among 9 possible positions.
//   for (let i = 1; i < layers; i++) {
//     push();
//     translate(width/2, height/2);
//     let j = pow(decr, i);
//     rotate(radians(cir+offset1*j));
//     stroke(str1/j, stralpha1);
//     fill(r1/j, g1/j, b1);
//     ellipse(posx1*j, posy1*j, size1*j, size1*j);  //the moving dot(s), the x is the distance from the center
//     pop();
//   }
// }











// v.1

// let num = 12;
// let layers = 30;
// let decr = 0.90;
// let pos = 250;
// let size = 90;
// let offset = 110;
//
// function setup() {
//   createCanvas(windowWidth, windowHeight);
//   background(10, 10, 255);
//   frameRate(10);
// }
//
// function draw() {
//   fill(20, 20, 10, 80);
//   noStroke();
//   rect(0, 0, width, height);
//
//   for (let i = 1; i < layers; i++) {
//     let j = pow(decr, i);
//     drawThrobber(num, pos*j, size*j, offset*j, 45/j, 20/j, 150/j)
//     wait  //pass to another function, try changing this number
//   }
// }
//
// function drawThrobber(num1, pos1, size1, offset1, r1, g1, str1) {
//   push();
//   translate(width/2, height/2); //move things to the center    // 360/num >> degree of each ellipse' move ;frameCount%num >> get the remainder that indicates the movement of the ellipse
//
//   let cir = 360/num1*(frameCount%num1);  //to know which one among 9 possible positions.
//
//   rotate(radians(cir+offset1));
//   stroke(str1,100);
//   fill(r1,g1,255);
//   ellipse(pos1, 0, size1, size1);  //the moving dot(s), the x is the distance from the center
//   pop();
//
//   // stroke(255,0,0);
//   // line(60,0,60,height);   //a static line
//   // line(width-60,0,width-60,height);   //a static line
// }
